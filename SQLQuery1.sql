CREATE TABLE [dbo].[page] (
    [pid]        INT          IDENTITY (1, 1) NOT NULL,
    [ptitle]     VARCHAR (50) NULL,
    [pcontent]   VARCHAR (50) NULL,
    [date]       VARCHAR (10) NULL,
    [publish]    BIT          DEFAULT ((1)) NULL,
    [authorname] VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([pid] ASC)
);
