﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="cmsAssignment._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
     <div class="jumbotron">
         <asp:Label ID="IDpage" runat="server" /> &nbsp;&nbsp;&nbsp;
    <asp:Label ID="PageName" runat="server" />&nbsp;&nbsp;&nbsp;
     <asp:Label ID="ContentPage" runat="server" />&nbsp;&nbsp;&nbsp;
    <asp:PlaceHolder ID="PlaceHolder1" runat="server" />

    <asp:Button id="NewPageAddBtn" text="Click To Add New Page" runat="server" OnClick="btnAdd_Click"/>
<br /> <br />


    <asp:GridView ID="gridviewPageList" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None"
        AutoGenerateColumns="false" ShowFooter="false" DataKeyNames="pid"
        OnRowEditing="gridviewPageList_RowEditing" OnRowCancelingEdit="gridviewPageList_RowCancelingEdit"
        OnRowUpdating="gridviewPageList_RowUpdating" OnRowCommand="gridviewPageList_RowCommand"
        OnRowDeleting="gridviewPageList_RowDeleting">


        <AlternatingRowStyle BackColor="White" />
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <SortedAscendingCellStyle BackColor="#FDF5AC" />
        <SortedAscendingHeaderStyle BackColor="#4D0000" />
        <SortedDescendingCellStyle BackColor="#FCF6C0" />
        <SortedDescendingHeaderStyle BackColor="#820000" />

        <Columns>

            <asp:TemplateField HeaderText="Page Name">
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("ptitle") %>' runat="server"></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtTitle" Text='<%# Eval("ptitle") %>' runat="server" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtTitleFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Content to be display">
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("pcontent") %>' runat="server"></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtContent" Text='<%# Eval("pcontent") %>' runat="server" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtContentFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Publish date">
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("date") %>' runat="server"></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtDate" Text='<%# Eval("date") %>' runat="server" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtContentFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Author Name">
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("authorname") %>' runat="server"></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtAuthorName" Text='<%# Eval("authorname") %>' runat="server" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtContentFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Published">
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("publish") %>' runat="server"></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:CheckBox ID="publish" Checked='<%# Eval("publish").ToString()=="True"  %>' runat="server" />        
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtContentFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

           <asp:TemplateField HeaderText="Operations">
                <ItemTemplate>
                    <asp:LinkButton text="Edit"  CommandName="Edit"   Width="70px" Height="30px" runat="server" />
                    <asp:LinkButton text="Delete" CommandName="Delete"  Width="70px" Height="30px" runat="server" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:LinkButton text="Save" CommandName="Update"   Width="70px" Height="30px" runat="server" />
                    <asp:LinkButton text="Cancel" CommandName="Cancel"  Width="70px" Height="30px" runat="server"  />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:LinkButton text="Proceed(ADD)" CommandName="AddNew" Width="130px" Height="30px" runat="server"  />
                </FooterTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
         </div>
    <div class="row">
        <div class="col-md-4">
            <h2>Getting started</h2>
            <p>
                ASP.NET Web Forms lets you build dynamic websites using a familiar drag-and-drop, event-driven model.
            A design surface and hundreds of controls and components let you rapidly build sophisticated, powerful UI-driven sites with data access.
            </p>
            <p>
                <a class="btn btn-default" href="https://go.microsoft.com/fwlink/?LinkId=301948">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Get more libraries</h2>
            <p>
                NuGet is a free Visual Studio extension that makes it easy to add, remove, and update libraries and tools in Visual Studio projects.
            </p>
            <p>
                <a class="btn btn-default" href="https://go.microsoft.com/fwlink/?LinkId=301949">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Web Hosting</h2>
            <p>
                You can easily find a web hosting company that offers the right mix of features and price for your applications.
            </p>
            <p>
                <a class="btn btn-default" href="https://go.microsoft.com/fwlink/?LinkId=301950">Learn more &raquo;</a>
            </p>
        </div>
    </div>

</asp:Content>
