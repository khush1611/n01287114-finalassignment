﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(cmsAssignment.Startup))]
namespace cmsAssignment
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
