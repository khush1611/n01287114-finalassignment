﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddNewPagesForm.aspx.cs" Inherits="cmsAssignment.AddNew" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
   
    <table>
        <tr>
            <td>
                <asp:Label ID="HiddenLabel" runat="server" /></td>
        </tr>
        <tr>
            <td>
                <asp:Label Text="Page" ID="lblName" runat="server" /></td>
            <td>
                <asp:TextBox ID="PageName" runat="server" /></td>
        </tr>
        <tr>
            <td>
                <asp:Label Text="Content" ID="lblContent" runat="server" /></td>
            <td>
                <asp:TextBox ID="ContentName" runat="server" /> </td>
        </tr>
        <tr>
            <td>
                <asp:Label Text="Date" ID="Label1" runat="server" /></td>
            <td>
                <asp:TextBox ID="TextBox_date" runat="server" /> </td>
        </tr>
         <tr>
            <td>
                <asp:Label Text="Do you want to publish it now?" ID="Label2" runat="server" /></td>
            <td>
               
                 <asp:RadioButton ID="radiobutton1" Text="Yes" GroupName="publish" runat="server"  /> 
                 <asp:Radiobutton Text="No" GroupName="publish" runat="server" /> 
             </td>
        </tr>

        <tr>
            <td>
                <asp:Label Text="Author Name" ID="Label3" runat="server" /></td>
            <td>
               
                  <asp:TextBox ID="TextBox_authorname" runat="server" /> 
             </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnAdd" Text="Add" runat="server" Height="45px" width="70px" OnClick="btnAdd_Click_Event" /></td>
        </tr>

    </table>
        </div>
</asp:Content>

