using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cmsAssignment
{
    public partial class AddNew : System.Web.UI.Page
    {
        string connection_string = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=H:\study\Web application development\cmsAssignment\cmsAssignment\App_Data\pageDatabase.mdf;Integrated Security=True";

        protected void Page_Load(object sender, EventArgs e)
        {
            HiddenLabel.Text = "";
            TextBox_date.Text= DateTime.Now.ToString("yyyy-MM-dd");
        }

        protected void btnAdd_Click_Event(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(connection_string);
            con.Open();

            if (PageName.Text != "" && ContentName.Text != "")
            {
                SqlCommand cmd = new SqlCommand("INSERT INTO page (ptitle,pcontent,authorname,date,publish) VALUES (@ptitle,@pcontent,@authorname,@date,@publish)", con);
                cmd.Parameters.AddWithValue("@ptitle", (PageName.Text.ToString()));
                cmd.Parameters.AddWithValue("@pcontent", (ContentName.Text.ToString()));
                cmd.Parameters.AddWithValue("@authorname", (TextBox_authorname.Text.ToString()));
                cmd.Parameters.AddWithValue("@date", (TextBox_date.Text.ToString()));
                cmd.Parameters.AddWithValue("@publish", (radiobutton1.Checked.ToString()));
                cmd.ExecuteNonQuery();
                con.Close();
                Response.Redirect("Default.aspx");
            }
            else
            {
                HiddenLabel.Text = "Please enter all detail";
            }
        }
    }
}