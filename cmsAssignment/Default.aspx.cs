using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cmsAssignment
{
    public partial class _Default : Page
    {
        string connection_string = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=H:\study\Web application development\cmsAssignment\cmsAssignment\App_Data\pageDatabase.mdf;Integrated Security=True";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["pageid"] == null)
            {
                gridviewPageList.Visible = true;
                PlaceHolder1.Visible = true;
                IDpage.Visible = false;
                PageName.Visible = false;
                ContentPage.Visible = false;
                NewPageAddBtn.Visible = true;
            }
            else
            {
                gridviewPageList.Visible = false;
                PlaceHolder1.Visible = false;
                NewPageAddBtn.Visible = false;

                IDpage.Visible = true;
                PageName.Visible = true;
                ContentPage.Visible = true;

                IDpage.Text = "<b><em>Page ID AUTO =</em></b>" + Request.QueryString["pageid"];
                PageName.Text = "<b><em>Page Name Entered=</em> </b>" + Request.QueryString["PageName"];
                ContentPage.Text = "<b><em>Data Entered = </em></b>" + Request.QueryString["PageContent"];

            }

            if (!this.IsPostBack)
            {
                PopulateGridView();
            }
        }

        private void PopulateGridView()
        {
            DataTable dt = fetchData();
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append("<table cellspacing=15 cellpadding=25>");
            stringBuilder.Append("<tr>");

            foreach (DataRow row in dt.Rows)
            {
             if (row[5].ToString() == "True")
                {
                    stringBuilder.Append("<td><h3><a href='Default.aspx?pageid=" + row[0] + "&PageName=" + row[1] + "&PageContent=" + row[2] + "'>");
                    stringBuilder.Append(row[1]);
                    stringBuilder.Append("</a></h3> &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp</td>");
                }
            }
            stringBuilder.Append("</tr>");
            stringBuilder.Append("</table>");

            PlaceHolder1.Controls.Add(new Literal { Text = stringBuilder.ToString() });

            if (dt.Rows.Count > 0)
            {
                gridviewPageList.DataSource = dt;
                gridviewPageList.DataBind();
            }
            else
            {
                dt.Rows.Add(dt.NewRow());
                gridviewPageList.DataSource = dt;
                gridviewPageList.DataBind();

                gridviewPageList.Rows[0].Cells.Clear();
                gridviewPageList.Rows[0].Cells.Add(new TableCell());
                gridviewPageList.Rows[0].Cells[0].ColumnSpan = dt.Columns.Count;
                gridviewPageList.Rows[0].Cells[0].Text = "No Data Found ..!";
                gridviewPageList.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
            }
        }
     
        public DataTable fetchData()
        {
            SqlConnection sqlcon = new SqlConnection(connection_string);
            sqlcon.Open();

            SqlCommand sqlcmd = new SqlCommand("SELECT pid, ptitle, pcontent,authorname,date,publish FROM page", sqlcon);
            SqlDataAdapter sda = new SqlDataAdapter();

            sda.SelectCommand = sqlcmd;

            DataTable dtTable = new DataTable();
            sda.Fill(dtTable);
            sqlcon.Close();

            return dtTable;
        }

        protected void gridviewPageList_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gridviewPageList.EditIndex = e.NewEditIndex;
            PopulateGridView();
        }

        protected void gridviewPageList_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gridviewPageList.EditIndex = -1;
            PopulateGridView();
        }

        protected void gridviewPageList_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            SqlConnection sqlcon = new SqlConnection(connection_string);
            sqlcon.Open();

            SqlCommand sqlcmd = new SqlCommand("UPDATE page SET ptitle='" + (gridviewPageList.Rows[e.RowIndex].FindControl("txtTitle") as TextBox).Text.Trim() + "',pcontent='" + (gridviewPageList.Rows[e.RowIndex].FindControl("txtContent") as TextBox).Text.Trim() + "', authorname='" + (gridviewPageList.Rows[e.RowIndex].FindControl("txtAuthorName") as TextBox).Text.Trim() + "',date='" + (gridviewPageList.Rows[e.RowIndex].FindControl("txtDate") as TextBox).Text.Trim() + "',publish='" + (gridviewPageList.Rows[e.RowIndex].FindControl("publish") as CheckBox).Checked + "' WHERE pid = " + Convert.ToInt32(gridviewPageList.DataKeys[e.RowIndex].Value.ToString()) + "", sqlcon);

            sqlcmd.ExecuteNonQuery();

            gridviewPageList.EditIndex = -1;
            PopulateGridView();

            sqlcon.Close();
        }

        protected void gridviewPageList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "New")
            {
                SqlConnection sqlcon = new SqlConnection(connection_string);
                sqlcon.Open();

                SqlCommand sqlcmd = new SqlCommand("INSERT INTO page (ptitle,pcontent) VALUES ('" + (gridviewPageList.FooterRow.FindControl("txtTitleFooter") as TextBox).Text.Trim() + "','" + (gridviewPageList.FooterRow.FindControl("txtContentFooter") as TextBox).Text.Trim() + "')", sqlcon);
                sqlcmd.ExecuteNonQuery();

                PopulateGridView();
                sqlcon.Close();
            }
        }

        protected void gridviewPageList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            SqlConnection sqlcon = new SqlConnection(connection_string);
            sqlcon.Open();

            SqlCommand sqlcmd = new SqlCommand("DELETE FROM page WHERE pid =" + Convert.ToInt32(gridviewPageList.DataKeys[e.RowIndex].Value.ToString()) + "", sqlcon);
            sqlcmd.ExecuteNonQuery();

            PopulateGridView();
            sqlcon.Close();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddNewPagesForm.aspx");
        }
    }
}